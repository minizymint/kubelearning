FROM node:12.16
WORKDIR /usr/app
COPY . /usr/app
RUN npm install
EXPOSE 3000
CMD ["node", "app.js"]